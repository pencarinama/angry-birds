﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{

    public SlingShooter slingShooter;
    public TrailController trailController;
    public List<Bird> birds;

    public List<Enemy> enemies;

    private bool _isEnded = false;
    public BoxCollider2D tapCollider;

    private Bird _shotBird;


    // Start is called before the first frame update
    void Start()
    {
        for(int i = 0; i < birds.Count; i++)
        {
            birds[i].OnBirdDestroyed += ChangeBird;
            birds[i].OnBirdShot += AssignTrail;
        }
    
        for(int i= 0; i< enemies.Count; i++)
        {
            enemies[i].onEnemyDestroyed += CheckGameEnd;

        }
        slingShooter.InitiateBird(birds[0]);

        tapCollider.enabled = false;
        _shotBird = birds[0];
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void CheckGameEnd(GameObject destroyedEnemy)
    {
        for(int i = 0; i < enemies.Count; i++)
        {
            if(enemies[i].gameObject == destroyedEnemy)
            {
                enemies.RemoveAt(i);
                break;
            }
        }

        if(enemies.Count <= 0)
        {
            _isEnded = true;
        }
    }

    public void ChangeBird()
    {
        tapCollider.enabled = false;
        if(_isEnded)
        {
            return;
        }
        birds.RemoveAt(0);
        if(birds.Count > 0)
        {
            slingShooter.InitiateBird(birds[0]);
            _shotBird = birds[0];
        }

       
    }

    public void AssignTrail(Bird bird)
    {
        trailController.SetBird(bird);
        StartCoroutine(trailController.SpawnTrail());

        tapCollider.enabled = true;
    }

    private void OnMouseUp()
    {
        if(_shotBird != null)
        {
            _shotBird.OnTap();
        }
    }
}
